#!/bin/bash

# Clean the files
rm -rf adsp/build
rm -rf cpu/build

mkdir -p adsp/build
mkdir -p cpu/build

cd adsp/build
cmake ../ -DQC_SOC_TARGET=APQ8096  -DCMAKE_TOOLCHAIN_FILE=../../../voxl-toolchain/Toolchain-hexagon-sdk3.1.cmake
make

cd ../../cpu/build
cmake ../ -DQC_SOC_TARGET=APQ8096  -DCMAKE_TOOLCHAIN_FILE=../../../voxl-toolchain/Toolchain-arm-linux-gnueabi.cmake
make

