# DSP Lib
project( version_test )
cmake_minimum_required(VERSION 3.4)
message( "Source directory ${PROJECT_SOURCE_DIR}")
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${PROJECT_SOURCE_DIR}/../../voxl-toolchain")
message( "Module path ${CMAKE_MODULE_PATH}")

include(qurt_flags)
include(qurt_lib)

FASTRPC_STUB_GEN( version_test.idl )

QURT_LIB(
	LIB_NAME version_test
	IDL_NAME version_test
	SOURCES version_test.c
	)
